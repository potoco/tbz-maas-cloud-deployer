<?php
  // Dashboard - index.php
?>
<?php

include('template-parts/header.php');

?>

<div class="content" style="background-color: #f4f3ef;">

  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-hat-3 text-warning"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Klasse</p>
                <p class="card-title"><?php echo ($resourcePool) ? strtoupper(explode('-', $resourcePool)[1]) : '-'; ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-app text-success"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Modul</p>
                <p class="card-title"><?php echo ($resourcePool) ? strtoupper(explode('-', $resourcePool)[0]) : '-'; ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-atom text-danger"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">MAAS Controller</p>
                <p class="card-title" style="font-size: 0.8em;"><?php echo $regionController->hostname; ?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-settings text-primary"></i>
              </div>
            </div>
            <div class="col-7 col-md-8 text-right">
              <div class="numbers">
                <p class="card-category">Lernumgebung</p>
              </div>
              <?php
                if (empty($machines))
                {
                  echo '<button type="button" class="btn btn-primary" style="margin-bottom:0;" onClick="createLernmaas()">Erstellen</button>';
                }
                else
                {
                  echo '<button type="button" class="btn btn-danger" style="margin-bottom:0;" onClick="deleteLernmaas()">Löschen</button>';
                }
              ?>
            </div>
          </div>
        </div>
        <div class="card-footer">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-plain">
        <div class="card-header">
          <h4 class="card-title">Übersicht MAAS Cloud</h4>
          <?php
            if (empty($machines))
              echo '<p>Erstelle eine neue Lernumgebung über den Aktionsbutton oben rechts</p>';
          ?>
        </div>

<?php if (!empty($machines)) { ?>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class="text-secondary">
                <th>Student</th>
                <th>Hostname</th>
                <th>IP-Adresse</th>
                <th>Power Status</th>
                <th class="text-right">Aktionen</th>
              </thead>
              <tbody>
<?php
                foreach ($machines as $machine)
                {
                  $studentName = (!empty($machine->description)) ? unserialize(base64_decode($machine->description))['name'] : '';
                  $hostname    = $machine->hostname;
                  $ipAddress   = (!empty($machine->description)) ? unserialize(base64_decode($machine->description))['ipaddr'] : '';
                  $powerState  = ucfirst($machine->power_state);
                  $actions     = '<btn class="btn btn-outline-success btn-round btn-icon" style="margin-left: 5px;" title="VM starten" x-data="'.$machine->system_id.'" onClick="powerOnMachine(this.getAttribute('."'x-data'".'))"><i class="nc-icon nc-button-play" style="font-weight: 900;"></i></btn>'.
                                 '<btn class="btn btn-outline-danger btn-round btn-icon" style="margin-left: 5px;" title="VM löschen" x-data="'.$machine->system_id.'" onClick="deleteMachine(this.getAttribute('."'x-data'".'))"><i class="nc-icon nc-simple-remove" style="font-weight: 900;"></i></btn>';

                  echo '<tr>';
                    echo '<td>', $studentName, '</td>';
                    echo '<td>', $hostname, '</td>';
                    echo '<td>', $ipAddress, '</td>';
                    echo '<td>', $powerState, '</td>';
                    echo '<td class="text-right">', $actions,'</td>';
                  echo '</tr>';
                }
?>
              </tbody>
            </table>
          </div>
        </div>
<?php } ?> 
      </div>
    </div>
  </div>

</div> <!--content-->

<?php include('template-parts/footer.php'); ?>
