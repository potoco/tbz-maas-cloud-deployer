<?php
include('template-parts/header.php');
?>

<div class="content" style="background-color: #f4f3ef;">

  <div id="selectcreation" class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Lernumgebung erstellen</h5>
          <p class="card-category">Was möchtest du tun?</p>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <div class="card" style="background-color: #fccf71;height: 250px;">
                <div class="card-header">
                  <h5 class="card-title">Kassenliste einlesen</h5>
                </div>
                <div class="card-body">
                  <p>Du kannst eine Klassenliste von <b>ecolm.com</b> (CSV Format) hochladen. Die Maschinen werden dann anhand dieser CSV-Datei erstellt.</p>
                  <form method="post" enctype="multipart/form-data">
                    <input id="csvFile" type="file" class="form-control" name="csvFile" style="background-color: #f4f3ef;">
                    <input id="createTyp-csv" type="hidden" name="createType" value="csvFile">
                    <button type="submit" class="btn btn-secondary" name="submit">CSV hochladen</button>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card" style="background-color: #7ed6a5;height: 250px;">
                <div class="card-header">
                  <h5 class="card-title">Manuell erstellen</h5>
                </div>
                <div class="card-body">
                  <p>Du kannst die Lernumgebung auch manuell erstellen, wenn du möchtest. Ohne weitere Angaben werden die Machinen aber nicht den Studenten zugewiesen und du musst diese selbst den Studenten zuweisen.</p>
                  <form method="post">
                    <input id="createTyp-man" type="hidden" name="createType" value="manual">
                    <button type="submit" class="btn btn-secondary" name="submit">Manuell erstellen</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<?php
// lernmaas creation by csv file
$createType = isset($_POST['createType']) ? $_POST['createType'] : '';
if (isset($_POST) && $createType == 'csvFile')
{
  if (isset($_FILES['csvFile']))
  {
    // validate input for manual creation
    $errors = [];
    $tmpFile = $_FILES['csvFile']['tmp_name'];
    $tmpFileMime = $_FILES['csvFile']['type'];
    $acceptedMimes = ['application/vnd.ms-excel','text/plain','text/csv','text/tsv'];
    if (in_array($tmpFileMime, $acceptedMimes))
    {
      $handle = fopen($_FILES['csvFile']['tmp_name'], 'r');
      if ($handle)
      {
        while (($line = fgets($handle)) !== false)
        {
          // process the lines, take only lines with email @
          if (preg_match('/@/', $line))
            $lines[] = $line;
        }
        fclose($handle);
      }
      else
      {
        $errors['csvFile'] = '<b>Fehler beim lesen der Datei:</b><br> - Datei konnte nicht geöffnet weden';
      }

      if (isset($lines))
      {
        // utf8 encoded: https://stackoverflow.com/questions/3800292/working-with-files-and-utf8-in-php
        // umlaute: https://www.php.de/forum/webentwicklung/php-einsteiger/php-tipps-2010/68154-erledigt-umlaute-ersetzen-preg_replace-funktioniert-nicht
        // schlägt fehl beim hinzufügen der describtion (assignStudents)
        $studentsArr = [];
        $umlaute = array("/ä/","/ö/","/ü/","/Ä/","/Ö/","/Ü/","/ß/");
        $replace = array("ae","oe","ue","Ae","Oe","Ue","ss");
        foreach ($lines as $line)
        {
          $student = [];
          $csvLine = str_getcsv($line, ';');
          // escape umlaute
          $student['name']    = preg_replace($umlaute, $replace, utf8_encode($csvLine[1]).' '.utf8_encode($csvLine[0]));
          $student['email']   = utf8_encode($csvLine[16]);
          if (preg_match('/@/', $student['email']))
            $studentsArr[] = $student;
        }
      }
    }
    else
    {
      $errors['csvFile'] = '<b>Fehler beim hochladen der Datei:</b><br> - Keine Datei ausgewählt<br> - Dateiformat ist kein CSV';
    }
  }

  if (isset($_POST['studentsArray']) && !isset($studentsArr))
    $studentsArr = unserialize($_POST['studentsArray']);
  ?>

  <div class="row" id="students-list">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Angaben aus der CSV-Datei</h5>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <?php
                if (!empty($errors) && !isset($studentsArr))
                {
                  foreach ($errors as $key => $message) echo '<div class="alert alert-danger"><span style="color: #252422;">'.$message.'</span></div>';
                }
                else
                {
              ?>
                  <div class="alert alert-info">
                    <span>Bitte kontrolliere die Angaben aus der CSV-Datei und gibt die Angaben zur Klasse weiter unten an.<br>
                    Sollte etwas nicht stimmen, kannst du jederzeit zu <b>manuell erstellen</b> wechseln</span>
                  </div>
                  <div class="table-responsive">
                    <table class="table">
                      <thead class="text-secondary">
                        <tr>
                          <th>ID</th>
                          <th>Name</th>
                          <th>E-Mail</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        foreach ($studentsArr as $index => $student)
                        {
                          echo '<tr>';
                            // sprintf - https://stackoverflow.com/questions/1699958/formatting-a-number-with-leading-zeros-in-php
                            $studentCount = ($index + 1);
                            echo '<td>'.sprintf('%02d', $studentCount).'</td>';
                            echo '<td>'.$student['name'].'</td>';
                            echo '<td>'.$student['email'].'</td>';
                          echo '</tr>';
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
              <?php
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
}
?>

<?php
// manual lernmaas creation
if (isset($_POST) && !empty($_POST))
{
  if (isset($_POST['className']) || isset($_POST['moduleName']) || isset($_POST['studentCount']))
  {
    // validate input for manual creation
    $className     = (strlen($_POST['className']) > 0 && strlen($_POST['className']) <= 15) ? $_POST['className'] : '';
    $moduleName    = (strlen($_POST['moduleName']) > 0 && strlen($_POST['moduleName']) <= 4) ? strtolower($_POST['moduleName']) : '';
    $studentCount  = ($_POST['studentCount'] > 0 && $_POST['studentCount'] <= 30) ? $_POST['studentCount'] : '';

    $errors = [];
    if (empty($className))
      $errors['className'] = 'Klassenname ist zu kurz oder lang';
    if (empty($moduleName) || !checkIctModule($moduleName) || !preg_match('/^(?i)m/', $moduleName))
      $errors['moduleName'] = 'Modul exisitiert nicht';
    if (empty($studentCount))
      $errors['studentCount'] = 'Muss zwischen <b>1</b> und <b>30</b> sein';
  }
?>
  <script type="text/javascript">
    $(document).ready(function(){
      $('html, body').animate({
          scrollTop: $(document).height()-$(window).height()
      }, 1000);
    });
  </script>
  <div id="classinfoform" class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Angaben zur Klasse</h5>
        </div>
        <div class="card-body">
          <form method="post">
            <div class="row">
              <div class="col-md-4 pr-1">
                <div class="form-group">
                  <label style="font-size: 1em;">Klasse</label>
                  <input type="text" class="form-control" placeholder="Name der Klasse" name="className" value="<?php echo (isset($className)) ? $className : '';?>">
                  <?php echo (isset($errors['className'])) ? '<small style="font-size: 1em;" id="moduleNameHelp" class="text-danger">'.$errors['className'].'</small>' : ''; ?> 
                </div>
              </div>
              <div class="col-md-4 px-1">
                <div class="form-group">
                  <label style="font-size: 1em;">Modul</label>
                  <input type="text" class="form-control" placeholder="Name des Moduls (z.B. M122)" name="moduleName" value="<?php echo (isset($moduleName)) ? $moduleName : '';?>">
                  <?php echo (isset($errors['moduleName'])) ? '<small style="font-size: 1em;" id="moduleNameHelp" class="text-danger">'.$errors['moduleName'].'</small>' : ''; ?> 
                </div>
              </div>
              <div class="col-md-4 pl-1">
                <div class="form-group">
                  <label style="font-size: 1em;">Anzahl Studenten</label>
                  <input type="number" class="form-control" placeholder="z.b 24" name="studentCount" value="<?php echo (isset($studentCount)) ? $studentCount : '';?>" <?php echo ($_POST['createType'] == 'csvFile') ? 'readonly' : ''; ?>>
                  <?php echo (isset($errors['studentCount'])) ? '<small style="font-size: 1em;" id="studentCountHelp" class="text-danger">'.$errors['studentCount'].'</small>' : ''; ?> 
                </div>
              </div>
            </div>
            <?php
            if (isset($studentsArr))
            {
            ?>
              <input id="createTyp-man" type="hidden" name="studentsArray" value="<?php echo htmlentities(serialize($studentsArr)); ?>">
            <?php
            }
            ?>
            <input id="createTyp-man" type="hidden" name="createType" value="<?php echo ($_POST['createType'] == 'manual') ? 'manual' : 'csvFile'; ?>">
            <button type="submit" class="btn btn-secondary float-right" name="createLernmaas">Lernumgebung erstellen</button>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php
}
?>

<?php
if (isset($_POST['createLernmaas']) && empty($errors))
{
?>
  <script type="text/javascript">
    $(document).ready(function() {
      // remove rows
      $('#classinfoform').hide()
      $('#students-list').hide()
      $('#selectcreation').hide()

      // create machines
      // 3-schuss-salven
      var defaultOffset = <?php echo DEFAULTOFFSET; ?>;
      var totalMachines = <?php echo $studentCount; ?>;
      var count = 0;
      var createCount = 3;
      const createMachines = async () => new Promise(resolve => {
        const interv = setInterval(() => {
          count += createCount;
          if (count > totalMachines)
          {
            createCount = totalMachines % createCount;
          }
          $.ajax({
            type: "POST",
            url: 'services/create_machines.php',
            data: 'action=create&module=<?php echo $moduleName; ?>&count='+createCount+'&className=<?php echo $className; ?>&offset='+defaultOffset,
            success: function(response) {
              try
              {
                let json = jQuery.parseJSON(response);
                resolve(json.message);
              }
              catch (error)
              {
                resolve('Fehler beim verarbeiten der Antwort: ' + error );
              }
            },
            error: function(error) {
              resolve('Fehler beim AJAX POST: ' + error);
            }
          });
          defaultOffset += createCount;
          if (count >= totalMachines)
          {          
            clearInterval(interv);
          }
        }, 3000)
      });
      // check ready status
      const waitForReadyStatus = async () => new Promise(resolve => {
        const interv = setInterval(() => {
          $.ajax({
            type: "POST",
            url: 'services/get_machinesStatus.php',
            data: 'action=getStatus',
            success: function(response) {
              let json = jQuery.parseJSON(response);
              if (json.message === 'Ready')
              {
                $('#statusmsgcreate').text(json.message);
                clearInterval(interv);
                resolve(json.message);
              }
              else
              {
                $('#statusmsgcreate').text(json.message);
              }
            },
            error: function(error) {
              resolve(error)
            }
          });
        }, 5000)
      });
      // tag machines
      const tagMachines = async () => new Promise(resolve => {
        $.ajax({
          type: "POST",
          url: 'services/do_tagMachines.php',
          data: 'action=tagMachines',
          success: function(response) {
            let json = jQuery.parseJSON(response);
            resolve(json.message);
          },
          error: function(error) {
            resolve(error)
          }
        });
      });
      // assign Zone
      const assignZone = async () => new Promise(resolve => {
        $.ajax({
          type: "POST",
          url: 'services/do_assignZone.php',
          data: 'action=assignZone',
          success: function(response) {
            let json = jQuery.parseJSON(response);
            resolve(json.message);
          },
          error: function(error) {
            resolve(error)
          }
        });
      });
<?php
if (!empty($studentsArr))
{
?>
      // assign students to machines
      const assignStudents = async () => new Promise(resolve => {
        $.ajax({
          type: "POST",
          url: 'services/do_assignStudents.php',
          data: 'action=assignStudents&studentsArr=<?php echo serialize($studentsArr); ?>',
          success: function(response) {
            let json = jQuery.parseJSON(response);
            resolve(json.message);
          },
          error: function(error) {
            resolve(error)
          }
        });
      });
<?php
}
?>
      // deploy machines
      const deployMachines = async () => new Promise(resolve => {
        $.ajax({
          type: "POST",
          url: 'services/deploy_machines.php',
          data: 'action=deploy',
          success: function(response) {
            let json = jQuery.parseJSON(response);
            resolve(json.message);
          },
          error: function(error) {
            resolve(error)
          }
        });
      });
      // check deployed status
      const waitForDeployedStatus = async () => new Promise(resolve => {
        const interv = setInterval(() => {
          $.ajax({
            type: "POST",
            url: 'services/get_machinesStatus.php',
            data: 'action=getStatus',
            success: function(response) {
              let json = jQuery.parseJSON(response);
              if (json.message === 'Deployed')
              {
                $('#statusmsgdeploy').text(json.message);
                clearInterval(interv);
                resolve(json.message);
              }
              else
              {
                $('#statusmsgdeploy').text(json.message);
              }
            },
            error: function(error) {
              resolve(error)
            }
          });
        }, 5000)
      });
      // lernmaas creation routine
      const process = async () => {
          console.log('starting lernmaas creation routine');
          await createMachines().then((message) => {
            if (message === "success")
            {
              $('#progressbar').css('width','25%');
              $('#status-info').append('<p>Maschinen wurden erstellt</p>');
              $('#status-info').append('<p>Status: <span id="statusmsgcreate">Status wird geprüft</span></p>');
              $error = 0;
            }
            else
            {
              $('#status-info').append('<p>' + message + '</p>');
              $error = 1;
            }
          });
          console.log('createMachines Done');
          if ($error === 0)
          {
            await waitForReadyStatus().then((message) => {
              if (message === "Ready")
              {
                $('#progressbar').css('width','40%');
                $('#status-info').append('<p>Maschinen werden getagged</p>');
                $error = 0;
              }
              else
              {
                $('#status-info').append('<p>' + message + '</p>');
                $error = 1;
              }
            });
          }
          console.log('waitForReadyStatus Done');
          if ($error === 0)
          {
            await tagMachines().then((message) => {
              if (message === "success")
              {
                $('#progressbar').css('width','55%');
                $('#status-info').append('<p>Maschinen werden der Availablity Zone zugewiesen</p>');
                $error = 0;
              }
              else
              {
                $('#status-info').append('<p>' + message + '</p>');
                $error = 1;
              }
            });
          }
          console.log('tagMachines Done');
          if ($error === 0)
          {
            await assignZone().then((message) => {
              if (message === "success")
              {
                $('#progressbar').css('width','70%');
<?php
if (!empty($studentsArr))
{
?>
                $('#status-info').append('<p>Maschinen werden den Studenten zugewiesen</p>');
<?php
}
else
{
?>
                $('#status-info').append('<p>Maschinen werden bereitgestellt</p>');
<?php
}
?> 
                $error = 0;
              }
              else
              {
                $('#status-info').append('<p>' + message + '</p>');
                $error = 1;
              }
            });
          }
          console.log('assignZone Done');
<?php
if (!empty($studentsArr))
{
?>
          if ($error === 0)
          {
            await assignStudents().then((message) => {
              if (message === "success")
              {
                $('#progressbar').css('width','80%');
                $('#status-info').append('<p>Maschinen werden bereitgestellt</p>');
                $error = 0;
              }
              else
              {
                $('#status-info').append('<p>' + message + '</p>');
                $error = 1;
              }
            });
          }
          console.log('assignStudents Done');
<?php
}
?>
          if ($error === 0)
          {
            await deployMachines().then((message) => {
              if (message === "success")
              {
                $('#progressbar').css('width','90%');
                $('#status-info').append('<p>Status: <span id="statusmsgdeploy">Status wird geprüft</span></p>');
                $error = 0;
              }
              else
              {
                $('#status-info').append('<p>' + message + '</p>');
                $error = 1;
              }
            });
          }
          console.log('deployMachines Done');
          if ($error === 0)
          {
            await waitForDeployedStatus().then((message) => {
              if (message === "Deployed")
              {
                $('#status-info').append('<p>Fertig!</p>');
                $('#progressbar').css('width','100%');
                $('#progressbar').removeClass('progress-bar-animated');
                $('#back-to-dashboard').fadeIn("slow");
                $error = 0;
              }
              else
              {
                $('#status-info').append('<p>' + message + '</p>');
                $error = 1;
              }
            });
          }
          console.log('all Done');
      }
      process();
    });
  </script>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Status Informationen</h5>
        </div>
        <div class="card-body">
          <div class="progress" style="margin-bottom: 20px;">
            <div id="progressbar" class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 1%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div id="status-info">
            <p>Lernumgebung wird erstellt...</p>
          </div>
          <button id="back-to-dashboard" type="button" class="btn btn-secondary" style="display:none;" onclick="window.location.href='/reload.php';">Zurück zum Dashboard</button>
        </div>
      </div>
    </div>
  </div>
<?php
}
?>

</div> <!--content-->

<?php include('template-parts/footer.php'); ?>