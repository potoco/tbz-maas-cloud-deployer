<?php

// Session Config
define('SESSIONEXPIRE',  3600);

// MAAS + Lernmaas Config
define('UBUNTUUSER',     'ubuntu');
define('MAASADMIN',      'ubuntu');
define('MAASURL',        'http://localhost:5240/MAAS/api/2.0');
define('MAASAPIKEY',     'yQcTSyDxUPJ...');

// VPN Config
define('VPNZONENAME',    '10-6-38-0');
define('VPNTAGNAME',     'wireguard');
define('DEFAULTOFFSET',  '10');
define('STUDENTSOFFSET', '50');

// SMTP Config
define('SMTPSERVER',   'smtp.domain.tld');
define('SMTPPORT',     '587');
define('SMTPAUTH',      true);
define('SMTPAUTHUSER', 'user');
define('SMTPAUTHPASS', 'pass');
define('MAILFROM',     'mail@domain.tld');
define('MAILFROMNAME', 'TBZ MAAS Cloud Deployer');
define('MAILCC',       ''); // Lehrperson oder Mailservice zwecks Kontrolle
define('MAILBCC',      ''); // Lehrperson oder Mailservice  zwecks Kontrolle
define('MAILTITLETXT', 'Deine Wireguard Config für das Modul'); // Modulname wird dynamisch geladen
define('MAILBODYTXT',  'Füge die Wireguard-Konfigurationsdatei in deinem Wireguard-Client hinzu.<br>Die IP-Adresse deiner Maschine lautet:');