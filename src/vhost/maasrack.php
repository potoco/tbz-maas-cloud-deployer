<?php
  // Dashboard - index.php
?>
<?php

include('template-parts/header.php');

if (isset($regionController) && !empty($regionController))
{
  $regHostname     = $regionController->hostname;
  $regVersion      = $regionController->version;
  $regNodeTypeName = $regionController->node_type_name;
  $regOs           = $regionController->osystem;
  $regArch         = $regionController->architecture;
  $regHwVendor     = $regionController->hardware_info->system_vendor;
  $regHwProduct    = $regionController->hardware_info->system_product;
  $regHwCpu        = $regionController->hardware_info->cpu_model;
  $regCpu_count    = $regionController->cpu_count;
  $regIpAddress    = $regionController->ip_addresses[0];
  $regMacAddress   = $regionController->interface_set[0]->mac_address;
  $regIntName      = $regionController->interface_set[0]->name;
  $regMemory       = ($regionController->memory / 1024);
}

?>

<div class="content" style="background-color: #f4f3ef;">

  <div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-atom text-danger"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">MAAS Controller</p>
                <p class="card-title" style="font-size: 0.8em;"><?php echo $regionController->hostname; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-settings-gear-65 text-warning"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">KVM Machinen</p>
                <p class="card-title"><?php echo ($kvmMachines) ? sizeof($kvmMachines) : '-'; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-chart-pie-36 text-success"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Resource Pool</p>
                <p class="card-title"><?php echo ($resourcePool) ? $resourcePool : '-'; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Regioncontroller</h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Hostname:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regHostname)) ? $regHostname : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Version:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regVersion)) ? $regVersion : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Node Typ:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regNodeTypeName)) ? $regNodeTypeName : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Betriebssystem:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regOs)) ? $regOs : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Architektur:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regArch)) ? $regArch : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Hersteller:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regHwVendor)) ? $regHwVendor : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Produkt:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regHwProduct)) ? $regHwProduct : ''; ?></span>
              </p>
            </div>
            <div class="col-md-6">
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Prozessor:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regHwCpu)) ? $regHwCpu : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Anzahl CPU:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regCpu_count)) ? $regCpu_count : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Memory:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regMemory)) ? $regMemory : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>Interface:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regIntName)) ? $regIntName : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>IP Adresse:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regIpAddress)) ? $regIpAddress : ''; ?></span>
              </p>
              <p>
                <span class="col-md-4" style="display: inline-block;"><b>MAC Adresse:</b></span>
                <span class="col-md-6" style="display: inline-block;"><?php echo (isset($regMacAddress)) ? $regMacAddress : ''; ?></span>
              </p>
            </div>
          </div>
        </div>
        <div class="card-footer"></div>
      </div>
    </div>
  </div>

<?php
if (isset($kvmMachines) && !empty($kvmMachines))
{
  $count = 0;
  foreach ($kvmMachines as $kvm)
  {
    $count += 1;
    $kvmHostname   = $kvm->hostname;
    $kvmOs         = $kvm->osystem;
    $kvmArch       = $kvm->architecture;
    $kvmSysId      = $kvm->system_id;
    $kvmHwKernel   = $kvm->hwe_kernel;
    $kvmHwVendor   = $kvm->hardware_info->system_vendor;
    $kvmHwProduct  = $kvm->hardware_info->system_product;
    $kvmHwCpu      = $kvm->hardware_info->cpu_model;
    $kvmCpu_count  = $kvm->cpu_count;
    $kvmMemory     = ($kvm->memory / 1024);
    $kvmDiskSize   = explode('.', (explode('.', ($kvm->storage))[0] / 1024))[0];
    $kvmIntName    = $kvm->interface_set[0]->name;
    $kvmIpAddress  = $kvm->ip_addresses[0];
    $kvmMacAddress = $kvm->interface_set[0]->mac_address;
?>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title">KVM Host <?php echo $count; ?></h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Hostname:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmHostname)) ? $kvmHostname : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Betriebssystem:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmOs)) ? $kvmOs : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Architektur:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmArch)) ? $kvmArch : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Kernel:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmHwKernel)) ? $kvmHwKernel : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>System ID:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmSysId)) ? $kvmSysId : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Hersteller:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmHwVendor)) ? $kvmHwVendor : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Produkt:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmHwProduct)) ? $kvmHwProduct : ''; ?></span>
                </p>
              </div>
              <div class="col-md-6">
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Prozessor:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmHwCpu)) ? $kvmHwCpu : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Anzahl CPU:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmCpu_count)) ? $kvmCpu_count : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Memory:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmMemory)) ? $kvmMemory : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Disk:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmDiskSize)) ? $kvmDiskSize : ''; ?> GB</span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>Interface:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmIntName)) ? $kvmIntName : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>IP Adresse:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmIpAddress)) ? $kvmIpAddress : ''; ?></span>
                </p>
                <p>
                  <span class="col-md-4" style="display: inline-block;"><b>MAC Addresse:</b></span>
                  <span class="col-md-6" style="display: inline-block;"><?php echo (isset($kvmMacAddress)) ? $kvmMacAddress : ''; ?></span>
                </p>
              </div>
            </div>
          </div>
          <div class="card-footer"></div>
        </div>
      </div>
    </div>
<?php
  }
}
?>

</div> <!--content-->

<?php include('template-parts/footer.php'); ?>
