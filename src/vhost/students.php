<?php
  // Dashboard - index.php
?>
<?php

include('template-parts/header.php');

?>

<div class="content" style="background-color: #f4f3ef;">

  <div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-hat-3 text-warning"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Klasse</p>
                <p class="card-title"><?php echo ($resourcePool) ? strtoupper(explode('-', $resourcePool)[1]) : '-'; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-app text-success"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Modul</p>
                <p class="card-title"><?php echo ($resourcePool) ? strtoupper(explode('-', $resourcePool)[0]) : '-'; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats" style="height: 100px;">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-atom text-danger"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">MAAS Controller</p>
                <p class="card-title" style="font-size: 0.8em;"><?php echo $regionController->hostname; ?><p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer ">
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-plain">
        <div class="card-header">
          <h4 class="card-title">Übersicht Studenten</h4>
          <?php
            if (empty($machines))
              echo '<p>Erstelle eine neue Lernumgebung über den Aktionsbutton oben rechts auf dem Dashboard</p>';
          ?>
        </div>

<?php if (!empty($machines)) { 
        if (!empty($machines[0]->description)) { ?>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class="text-secondary">
                  <th>Name</th>
                  <th>E-Mail</th>
                  <th>Hostname</th>
                  <th>IP-Adresse</th>
                  <th class="text-right">Aktionen</th>
                </thead>
                <tbody>
  <?php
                  $allEmail = [];
                  $module = strtoupper(explode('-', $resourcePool)[0]);
                  foreach ($machines as $machine)
                  {
                    $studentName = (!empty($machine->description)) ? unserialize(base64_decode($machine->description))['name'] : '';
                    $hostname    = $machine->hostname;
                    $ipAddress   = (!empty($machine->description)) ? unserialize(base64_decode($machine->description))['ipaddr'] : '';
                    $email       = (!empty($machine->description)) ? unserialize(base64_decode($machine->description))['email'] : '';
                    $studentVpnConfigId = explode('.', $ipAddress);
                    (int)$studentVpnConfigId = end($studentVpnConfigId);
                    $studentVpnConfigId = ($studentVpnConfigId + DEFAULTOFFSET + 30);
                    $studentVpnConfig   = $studentVpnConfigId.'.conf';
                    $actions     = '<btn class="btn btn-outline-info btn-round btn-icon" style="margin-left: 5px;" title="VPN Config herunterladen" onClick="window.open(\'/downloads/'.$studentVpnConfig.'\', \'_blank\')"><i class="nc-icon nc-cloud-download-93" style="font-weight: bold;"></i></btn>'.
                                   '<btn class="btn btn-outline-info btn-round btn-icon" style="margin-left: 5px;" title="VPN Config senden" x-data="module='.$module.'&email='.$email.'&vpnId='.$studentVpnConfigId.'&ipAddr='.$ipAddress.'" onClick="sendMail(this.getAttribute('."'x-data'".'))"><i class="fa fa-envelope"></i></btn>';

                    echo '<tr>';
                      echo '<td>', $studentName, '</td>';
                      echo '<td>', $email, '</td>';
                      echo '<td>', $hostname, '</td>';
                      echo '<td>', $ipAddress, '</td>';
                      echo '<td class="text-right">', $actions,'</td>';
                    echo '</tr>';
                  }
  ?>
                </tbody>
              </table>
            </div>
          </div>
<?php   }
      } ?> 
      </div>
    </div>
  </div>

</div> <!--content-->

<?php include('template-parts/footer.php'); ?>
