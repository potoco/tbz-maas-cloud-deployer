<?php
include('template-parts/header.php');
?>

<?php
if (isset($_GET) && $_GET['action'] == 'delete')
{
?>
<script type="text/javascript">
  $(document).ready(function() {
    // delete machines
    const deleteMachines = async () => new Promise(resolve => {
      $.ajax({
        type: "POST",
        url: 'services/delete_machines.php',
        data: 'action=delete',
        success: function(response) {
          let json = jQuery.parseJSON(response);
          resolve(json.message);
        },
        error: function(error) {
          resolve(error)
        }
      });
    });
    // delete resource pool
    const deleteResPool = async () => new Promise(resolve => {
      $.ajax({
        type: "POST",
        url: 'services/delete_respool.php',
        data: 'action=delete',
        success: function(response) {
          let json = jQuery.parseJSON(response);
          resolve(json.message);
        },
        error: function(error) {
          resolve(error)
        }
      });
    });
    // lernmaas deletion routine
    const process = async () => {
      console.log('starting lernmaas deletion routine');
      await deleteMachines().then((message) => {
        if (message === "success")
        {
          // update status info
          $('#progressbar').css('width','50%');
          $('#status-info').append('<p>Maschinen wurden gelöscht</p>'); 
          $error = 0;
        }
        else
        {
          $('#status-info').append('<p>' + message + '</p>');
          $error = 1;
        }
      });
      console.log('deleteMachines Done');
      await deleteResPool().then((message) => {
        if (message === "success")
        {
          // update status info
          $('#progressbar').css('width','80%');
          $('#status-info').append('<p>Ressourcenpool wurde gelöscht</p>');
          $('#status-info').append('<p>Fertig!</p>');
          $('#progressbar').css('width','100%');
          $('#progressbar').removeClass('progress-bar-animated');
          $('#back-to-dashboard').fadeIn("slow");
          $error = 0;
        }
        else
        {
          $('#status-info').append('<p>' + message + '</p>');
          $error = 1;
        }
      });
      console.log('deleteResPool Done');
      console.log('all Done');
    }
    process();
  });
</script>

<div class="content" style="background-color: #f4f3ef;">

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Status Informationen</h5>
        </div>
        <div class="card-body">
          <div class="progress" style="margin-bottom: 20px;">
            <div id="progressbar" class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 1%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <div id="status-info">
            <p>Lernumgebung wird gelöscht...</p>
          </div>
          <button id="back-to-dashboard" type="button" class="btn btn-secondary" style="display:none;" onclick="window.location.href='/reload.php';">Zurück zum Dashboard</button>
        </div>
        <div class="card-footer"></div>
      </div>
    </div>
  </div>

</div> <!--content-->

<?php
}
else
{
  echo '<div class="content">';
  echo  '<p>Nichts zu tun...';
  echo '</div> <!--content-->';
}
?>


<?php include('template-parts/footer.php'); ?>