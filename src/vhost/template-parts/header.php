<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');

checksession();

$regionController = (isset($_SESSION['regionController'])) ? unserialize($_SESSION['regionController']) : null;
$resourcePool     = (isset($_SESSION['resourcePool'])) ? unserialize($_SESSION['resourcePool']) : null;
$kvmMachines      = (isset($_SESSION['kvmMachines'])) ? unserialize($_SESSION['kvmMachines']) : null;
$machines         = (isset($_SESSION['machines'])) ? unserialize($_SESSION['machines']) : null;

$activeSite = $_SERVER['REQUEST_URI'];

?>
<!--
=========================================================
* Paper Dashboard 2 - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/paper-dashboard-2
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    TBZ MAAS Cloud Deployer
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!-- Fonts and icons -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="./assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet" />
  <link href="./assets/css/custom.css" rel="stylesheet" />
  <!-- Custom Javascript -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="./assets/js/custom.js"></script>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <div class="logo">
        <a href="/" class="simple-text logo-normal">
          <div class="logo-image-big">
            <img src="./assets/img/tbz-logo-small.png" style="padding: 0 60px;">
          </div>
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li <?php echo ($activeSite == '/') ? 'class="active"' : ''; ?>>
            <a href="/">
              <i class="nc-icon nc-tile-56"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li <?php echo ($activeSite == '/students.php') ? 'class="active"' : ''; ?>>
            <a href="/students.php">
              <i class="nc-icon nc-single-02"></i>
              <p>Studenten</p>
            </a>
          </li>
          <li <?php echo ($activeSite == '/maasrack.php') ? 'class="active"' : ''; ?>>
            <a href="/maasrack.php">
              <i class="nc-icon nc-alert-circle-i"></i>
              <p>MAAS Rack</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel" style="min-height: 100vh;">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">TBZ MAAS Cloud Deployer</a>
          </div>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a id="reload-link" class="nav-link btn-magnify" href="/reload.php" onclick="$('#reload-link-icon').addClass('rotate-this')">
                  <i id="reload-link-icon" class="fa fa-refresh" aria-hidden="true" style="margin-right: 5px;"></i>
                  Reload Lernumgebung
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->