<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');

if (isset($_POST) && $_POST['action'] == 'delete')
{
  $poolName = getRessourcePoolByZoneName(UBUNTUUSER, MAASADMIN, VPNZONENAME);
  $machines = getMachinesByPoolName(UBUNTUUSER, MAASADMIN, $poolName);
  if (!empty($machines))
  {
    $return['message'] = 'success';
    foreach ($machines as $machine)
    {
      $deleted = deleteMachine(UBUNTUUSER, MAASADMIN, $machine->system_id);  
      if ($deleted === true)
      {
        continue;
      }
      else
      {
        $return['message'] = 'Ein Fehler beim Löschen der Maschinen ist aufgetreten: '.$deleted;
        break;
      }
    }
  }
}

print json_encode($return);