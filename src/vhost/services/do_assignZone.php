<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');


if (isset($_POST) && $_POST['action'] == 'assignZone')
{
  $assignZone = assignZone(UBUNTUUSER, MAASADMIN, VPNZONENAME);
  if ($assignZone === 0)
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler bei der Zonenzuweisung der Maschinen ist aufgetreten: '.$assignZone;
  }
}

print json_encode($return);