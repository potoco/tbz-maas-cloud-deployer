<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');


if (isset($_POST) && $_POST['action'] == 'create')
{
  $createLernmaas = createLernmaas(UBUNTUUSER, MAASADMIN, $_POST['module'], $_POST['count'], $_POST['className'], $_POST['offset']);
  if ($createLernmaas === 0)
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler beim Erstellen der Maschinen ist aufgetreten: '.$createLernmaas;
  }
}

print json_encode($return);