<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');


if (isset($_POST) && $_POST['action'] == 'getStatus')
{
  $return['message'] = getMachinesStatus(UBUNTUUSER, MAASADMIN);
}

print json_encode($return);