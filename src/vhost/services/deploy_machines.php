<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');


if (isset($_POST) && $_POST['action'] == 'deploy')
{
  $deployMachines = deployMachines(UBUNTUUSER, MAASADMIN);
  if ($deployMachines === 0)
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler beim Bereitstellen der Maschinen ist aufgetreten: '.$deployMachines;
  }
}

print json_encode($return);