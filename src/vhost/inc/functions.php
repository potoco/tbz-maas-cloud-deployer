<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');

require $_SERVER['DOCUMENT_ROOT'].'/inc/phpmailer/src/Exception.php';
require $_SERVER['DOCUMENT_ROOT'].'/inc/phpmailer/src/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'].'/inc/phpmailer/src/SMTP.php';

// Quelle: https://github.com/PHPMailer/PHPMailer
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function sendMail(array $mailTo, $mailSubject, $mailBody, array $fileAttachment = null)
{
  // Instantiation and passing `true` enables exceptions
  $mail = new PHPMailer(true);

  try {
    // charset
    $mail->CharSet = 'utf-8';
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = SMTPSERVER;                             // Set the SMTP server to send through
    $mail->SMTPAuth   = SMTPAUTH;                               // Enable SMTP authentication
    $mail->Username   = SMTPAUTHUSER;                           // SMTP username
    $mail->Password   = SMTPAUTHPASS;                           // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = SMTPPORT;                               // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom(MAILFROM, MAILFROMNAME);
    foreach ($mailTo as $recipient)
      $mail->addAddress($recipient);                            // Add a recipient
    // $mail->addAddress('ellen@example.com', 'Ellen Harper');  // Name is optional
    $mail->addReplyTo(MAILFROM);
    if (!empty(MAILCC))
      $mail->addCC(MAILCC);
    if (!empty(MAILBCC))
      $mail->addBCC(MAILBCC);

    // Attachments
    if (isset($fileAttachment))
    {
      foreach ($fileAttachment as $fileName => $filePath)
      {
        $mail->addAttachment($filePath, $fileName);             // Add attachments
      }
    }

    // Content
    $mail->isHTML(true);                                        // Set email format to HTML
    $mail->Subject = $mailSubject;
    $mail->Body    = $mailBody;
    $mail->AltBody = strip_tags($mailBody);

    $mail->send();
    return $mail;
  } catch (Exception $e) {
    return $mail->ErrorInfo;
  }
}

function checksession()
{
  session_start();
  if (isset($_SESSION['timestamp']))
  {
    if ($_SESSION['timestamp'] < time() - SESSIONEXPIRE)
    { 
      // remove cookie
      setcookie(session_name(), '', 0);
      // destroy session
      session_destroy();
      // redirect to dashboard to create new session
      header('Location: /');
    }
    else
    {
      $_SESSION['timestamp'] = time();
    }
  } 
  else
  {
    $maasLogin = doMaasLogin(UBUNTUUSER, MAASADMIN, MAASURL, MAASAPIKEY);

    if ($maasLogin === 0)
    {
      $regionController = getRegionController(UBUNTUUSER, MAASADMIN)[0];
      $resourcePool     = getRessourcePoolByZoneName(UBUNTUUSER, MAASADMIN, VPNZONENAME);
      $kvmMachines      = getKvmMachines(UBUNTUUSER, MAASADMIN);
      $machines         = getMachinesByPoolName(UBUNTUUSER, MAASADMIN, $resourcePool);
      $prepareVpnConfig = prepareVpnConfig();
    }
    else
    {
      $regionController = new StdClass();
      $regionController->hostname = 'Fehler';
    }

    $_SESSION['regionController'] = (isset($regionController)) ? serialize($regionController) : null;
    $_SESSION['resourcePool']     = (isset($resourcePool)) ? serialize($resourcePool) : null;
    $_SESSION['kvmMachines']      = (isset($kvmMachines)) ? serialize($kvmMachines) : null;
    $_SESSION['machines']         = (isset($machines)) ? serialize($machines) : null;
    
    $_SESSION['timestamp'] = time();
  }
  return;
}

function prepareVpnConfig()
{
  $wireguardTgz = 'downloads/wireguard_configs.tgz';
  $extractDir   = explode('/', $wireguardTgz)[0].'/';

  $machines = getMachinesByZoneName(UBUNTUUSER, MAASADMIN, VPNZONENAME);

  if (!empty($machines))
  {
    // write tgz file to filesystem
    if (!file_exists($wireguardTgz))
    {
        // get base64 encoded tgz file stored in each machines description
        $wgRawBase64 = $machines[0]->zone->description;
        // get data from base64 encoded string
        $wgRawData = base64_decode($wgRawBase64);

        // save data into file
        $tgzFile   = fopen($wireguardTgz, 'w');
        $writeFile = fwrite($tgzFile, $wgRawData);
        $closeFile = fclose($tgzFile);
    }
    // extract all wireguard config files (<IP>.conf)
    if (!file_exists($extractDir.'02.conf'))
    {
      try {
          $phar = new PharData($wireguardTgz);
          $phar->extractTo($extractDir);
      } catch (Exception $e) {
          return $e;
      }
    }
  }
  return true;
}

function doMaasLogin($runAsUser, $maasCliAdmin, $maasUrl, $apiToken)
{
  $cmd = "sudo -u $runAsUser maas login $maasCliAdmin $maasUrl $apiToken";
  exec($cmd, $output);

  if (preg_match('/You are now logged in to the MAAS server at/', $output[1]))
    return 0;

  return 1;
}

function formatResponse($response)
{
  $uglyJson = '';
  foreach ($response as $line)
  {
    // sanitize output - createvms
    $line = (strpos($line, 'pod compose') && !preg_match("/\{|\}/", $line)) ? '' : $line;
    $line = (strpos($line, 'pod compose')) ? preg_replace("/\}.*/", '},', $line) : $line;

    $uglyJson .= preg_replace('/\}\s+\{/', '', $line);
  }

  // sanitize output - createvms
  $uglyJson = (!preg_match('/^\[/', $uglyJson)) ? '['.$uglyJson : $uglyJson;
  $uglyJson = (!preg_match('/\]$/', $uglyJson)) ? $uglyJson.']' : $uglyJson;

  return json_decode($uglyJson);
}

function getRegionController($runAsUser, $maasCliAdmin)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin region-controllers read";
  exec($cmd, $output);

  $response = formatResponse($output);

  return $response;
}

function getRessourcePool($runAsUser, $maasCliAdmin)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin resource-pools read";
  exec($cmd, $output);

  $response = formatResponse($output);

  foreach ($response as $resourcePool)
      $resPoolName = ($resourcePool->name !== 'default') ? $resourcePool->name : null;

  return $resPoolName;
}

function getRessourcePoolByZoneName($runAsUser, $maasCliAdmin, $zoneName)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin resource-pools read";
  exec($cmd, $output);

  $response = formatResponse($output);

  foreach ($response as $resourcePool)
  {
    if ($resourcePool->description == $zoneName)
    {
      return $resourcePool->name;
    }
  }

  return null;
}

function getRessourcePoolByName($runAsUser, $maasCliAdmin, $poolName)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin resource-pools read";
  exec($cmd, $output);

  $response = formatResponse($output);

  foreach ($response as $resourcePool)
      $resPoolName = ($resourcePool->name == $poolName) ? $resourcePool->name : null;

  return $resPoolName;
}

function mapRessourcePoolToZoneName($runAsUser, $maasCliAdmin, $poolName, $zoneName)
{
  // maas ubuntu resource-pool update m122-testdev description='10-6-38-0'
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin resource-pool update $poolName description='$zoneName'";
  exec($cmd, $output);

  $response = formatResponse($output);

  foreach ($response as $pool)
    if ($pool->description !== $zoneName) return 1;

  return 0;
}

function getMachines($runAsUser, $maasCliAdmin)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machines read";
  exec($cmd, $output);

  $response = formatResponse($output);

  $machines = [];
  foreach ($response as $machine)
  {
    if (!$machine->locked)
    {
      $machines[] = $machine;
    }
  }

  // sort machines array
  // Quelle: https://stackoverflow.com/questions/1597736/how-to-sort-an-array-of-associative-arrays-by-value-of-a-given-key-in-php
  $hostname = array_column($machines, 'hostname');
  array_multisort($hostname, SORT_ASC, $machines);

  return $machines;
}

function getMachinesByZoneName($runAsUser, $maasCliAdmin, $zoneName)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machines read";
  exec($cmd, $output);

  $response = formatResponse($output);

  $machines = [];
  foreach ($response as $machine)
  {
    if (!$machine->locked && $machine->zone->name === $zoneName)
    {
      $machines[] = $machine;
    }
  }

  // sort machines array
  // Quelle: https://stackoverflow.com/questions/1597736/how-to-sort-an-array-of-associative-arrays-by-value-of-a-given-key-in-php
  $hostname = array_column($machines, 'hostname');
  array_multisort($hostname, SORT_ASC, $machines);

  return $machines;
}

function getMachinesByPoolName($runAsUser, $maasCliAdmin, $poolName)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machines read";
  exec($cmd, $output);

  $response = formatResponse($output);

  $machines = [];
  foreach ($response as $machine)
  {
    if (!$machine->locked && $machine->pool->name === $poolName)
    {
      $machines[] = $machine;
    }
  }

  // sort machines array
  // Quelle: https://stackoverflow.com/questions/1597736/how-to-sort-an-array-of-associative-arrays-by-value-of-a-given-key-in-php
  $hostname = array_column($machines, 'hostname');
  array_multisort($hostname, SORT_ASC, $machines);

  return $machines;
}

function getMachinesByModuleAndClassName($runAsUser, $maasCliAdmin, $moduleName, $className)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machines read";
  exec($cmd, $output);

  $response = formatResponse($output);

  $machines = [];
  foreach ($response as $machine)
  {
    if (!$machine->locked && strpos($machine->hostname, $moduleName) && strpos($machine->hostname, $className))
    {
      $machines[] = $machine;
    }
  }

  // sort machines array
  // Quelle: https://stackoverflow.com/questions/1597736/how-to-sort-an-array-of-associative-arrays-by-value-of-a-given-key-in-php
  $hostname = array_column($machines, 'hostname');
  array_multisort($hostname, SORT_ASC, $machines);

  return $machines;
}

function getKvmMachines($runAsUser, $maasCliAdmin)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machines read";
  exec($cmd, $output);

  $response = formatResponse($output);

  $machines = [];
  foreach ($response as $machine)
  {
    if ($machine->locked)
    {
      $machines[] = $machine;
    }
  }
  return $machines;
}

function getMachineInfo($runAsUser, $maasCliAdmin, $sysId)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machine read $sysId";
  exec($cmd, $output);

  $response = formatResponse($output);

  $machine = new StdClass();
  $machine->hostname                  = $response->hostname;
  $machine->power_state               = $response->power_state;
  $machine->fqdn                      = $response->fqdn;
  $machine->locked                    = $response->locked;
  $machine->status                    = $response->status;
  $machine->status_name               = $response->status_name;
  $machine->commissioning_status_name = $response->commissioning_status_name;
  $machine->testing_status_name       = $response->testing_status_name;
  $machine->system_id                 = $response->system_id;
  $machine->zone                      = $response->zone->name;
  $machine->wireguard_config_all      = $response->zone->desc;
  $machine->ip_address                = $response->ip_addresses[0];
  $machine->status_message            = $response->status_message;
  $machine->tag_names                 = $response->tag_names;
  $machine->describtion               = $response->describtion;

  return $machine;
}

function deleteMachine($runAsUser, $maasCliAdmin, $sysId)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machine delete $sysId";
  exec($cmd, $output);

  // if deletion is successfull "true" is returned, else cli output error msg is returned
  return (!empty($output[0])) ? $output[0] : empty($output);
}

function deleteResourcePool($runAsUser, $maasCliAdmin, $resourcePoolName)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin resource-pool delete $resourcePoolName";
  exec($cmd, $output);

  // if deletion is successfull "true" is returned, else cli output error msg is returned
  return (!empty($output[0])) ? $output[0] : empty($output);
}

function deleteLernmaas($runAsUser, $maasCliAdmin)
{
  // get existing machines in pool and delete them
  $poolName = getRessourcePoolByZoneName($runAsUser, $maasCliAdmin, VPNZONENAME);
  $machines = getMachinesByPoolName($runAsUser, $maasCliAdmin, $poolName);
  if (!empty($machines))
  {
    foreach ($machines as $machine)
    {
      if (deleteMachine($runAsUser, $maasCliAdmin, $machine->system_id)) continue;
      return 1;
    }
  }

  // delete existing resource pool
  $resPools = getRessourcePool($runAsUser, $maasCliAdmin);
  if (!empty($resPools))
  {
    foreach ($resPools as $resPool)
    {
      if (deleteResourcePool($runAsUser, $maasCliAdmin, $resPool)) continue;
      return 1;
    }
  }

  return 0;
}

function createLernmaas($runAsUser, $maasCliAdmin, $module, $count, $className, $offset = DEFAULTOFFSET)
{
  // set environment variable for createvms script
  putenv("PROFILE=$runAsUser");
  $cmd = "sudo -E -u $runAsUser createvms /home/$runAsUser/lernmaas/config.yaml "
         .escapeshellarg($module)." "
         .escapeshellarg($count)." "
         .escapeshellarg($className)." "
         .escapeshellarg($offset);
  exec($cmd, $output);

  $response = formatResponse($output);

  // shift up pool creation - otherwise foreach system_id check fails
  if ($offset == DEFAULTOFFSET)
  {
    $resPool = array_shift($response);
    if (empty($resPool->name))
    {
      return $response;
    }
    else
    {
      $mapPooltoZone = mapRessourcePoolToZoneName($runAsUser, $maasCliAdmin, $resPool->name, VPNZONENAME);
      if ($mapPooltoZone !== 0)
        return $mapPooltoZone;
    }
  }

  // if machine system_id is empty => return cli response
  foreach ($response as $createdMachine)
  {
    if (empty($createdMachine->system_id))
      return $response;
  }
  return 0;
}

function checkReadyStatus($runAsUser, $maasCliAdmin)
{
  // check status "ready" to continue with next steps
  // $machine->status_message 'Ready' = $machine->status '4'
  $machines = getMachines($runAsUser, $maasCliAdmin);
  foreach ($machines as $machine)
    $ready = ($machine->status_message == 'Ready') ? true : false;
  return $ready;
}

function tagMachines($runAsUser, $maasCliAdmin, $tagName)
{
  $poolName = getRessourcePoolByZoneName($runAsUser, $maasCliAdmin, VPNZONENAME);
  $machines = getMachinesByPoolName($runAsUser, $maasCliAdmin, $poolName);
  foreach ($machines as $machine)
  {
    // reset $output, otherwise new output from exec() is beeing appended to $output
    $output = null;
    if (!in_array($tagName, $machine->tag_names))
    {
      $cmd = "sudo -u $runAsUser maas $maasCliAdmin tag update-nodes $tagName add=$machine->system_id";
      exec($cmd, $output);

      $response = formatResponse($output);

      if ($response[0]->added) continue;
      return 1;
    }
  }
  return 0;
}

function assignZone($runAsUser, $maasCliAdmin, $vpnZoneName)
{
  // get machines by pool name
  $poolName = getRessourcePoolByZoneName($runAsUser, $maasCliAdmin, $vpnZoneName);
  $machines = getMachinesByPoolName($runAsUser, $maasCliAdmin, $poolName);

  // get first available zone that match IP pattern - ex. 10-1-38-0
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin zones read";
  exec($cmd, $output);

  $response = formatResponse($output);

  foreach ($response as $zone)
  {
    if (preg_match('/^(?:[0-9]{1,3}\-){3}[0-9]{1,3}$/', $zone->name) && $zone->name === $vpnZoneName)
    {
      $zoneName = $zone->name;
      break;
    }
  }

  // add machine to wireguard vpn availability zone
  foreach ($machines as $machine)
  {
    // reset $output, otherwise new output from exec() is beeing appended to $output
    $output = null;
    if ($machine->zone->name !== $zoneName)
    {
      $cmd = "sudo -u $runAsUser maas $maasCliAdmin machine update $machine->system_id zone=$zoneName";
      exec($cmd, $output);

      $response = formatResponse($output);

      if ($response[0]->zone->name == $zoneName) continue;
      return 1;
    }
  }
  return 0;
}

function addMachineDescription($runAsUser, $maasCliAdmin, $sysId, $desc)
{
  // maas $PROFILE machine update tm3xfs description=test
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machine update $sysId description='$desc'";
  exec($cmd, $output);

  $response = formatResponse($output);

  if ($response[0]->description !== $desc)
    return 1;

  return 0;
}

function deployMachines($runAsUser, $maasCliAdmin)
{
  $poolName = getRessourcePoolByZoneName($runAsUser, $maasCliAdmin, VPNZONENAME);
  $machines = getMachinesByPoolName($runAsUser, $maasCliAdmin, $poolName);
  foreach ($machines as $machine)
  {
    if ($machine->status_message == 'Ready')
    {
      // reset $output, otherwise new output from exec() is beeing appended to $output
      $output = null;
      $cmd = "sudo -u $runAsUser maas $maasCliAdmin machine deploy $machine->system_id";
      exec($cmd, $output);

      $response = formatResponse($output);

      if (!empty($response)) continue;
      return 1;
    }
  }
  return 0;
}

function checkDeployedStatus($runAsUser, $maasCliAdmin)
{
  $machines = getMachines($runAsUser, $maasCliAdmin);
  foreach ($machines as $machine)
    $deployed = ($machine->status_message == 'Deployed') ? true : false;
  return $deployed;
}

function getMachinesStatus($runAsUser, $maasCliAdmin)
{
  $poolName = getRessourcePoolByZoneName($runAsUser, $maasCliAdmin, VPNZONENAME);
  $machines = getMachinesByPoolName($runAsUser, $maasCliAdmin, $poolName);
  $statusArr = [];
  foreach ($machines as $machine)
    $statusArr[] = $machine->status_message;

  $statusArr = array_unique($statusArr);

  if (sizeof($statusArr) == 1)
  {
    return $statusArr[0];
  }
  else
  {
    foreach ($statusArr as $status)
      if ($status !== 'Ready' && $status !== 'Deployed' ) return $status;
  }
}

function powerOnMachine($runAsUser, $maasCliAdmin, $sysId)
{
  $cmd = "sudo -u $runAsUser maas $maasCliAdmin machine power-on $sysId";
  exec($cmd, $output);
  return 0;
}

function checkIctModule($module)
{
  $module = urlencode(preg_replace('/^(?i)m/', '', $module));
  $url = "https://cf.ict-berufsbildung.ch/modules.php?name=Mbk&a=20101&cmodnr=$module";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, true);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch, CURLOPT_TIMEOUT,10);
  $response = curl_exec($ch);
  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);

  // if heading <h1> with module number exists => ict module is valid
  $moduleExists = (bool)preg_match('/<h1>'.$module.'\s/', $response);

  return $moduleExists;
}