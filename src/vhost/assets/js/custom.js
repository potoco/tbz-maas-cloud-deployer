// custom.js

function deleteLernmaas()
{
  var ask = window.confirm("Bist du dir sicher, dass du diese Lernumgebung löschen möchtest?");
  if (ask) {
    window.location.href = "deleteLernmaas.php?action=delete";
  }
}

function createLernmaas()
{
  window.location.href = "createLernmaas.php"
}

function showNotification(type, from, align, message)
{
  color = type;
  $.notify({
    icon: "nc-icon nc-sound-wave",
    message: message
  }, {
    type: color,
    timer: 8000,
    placement: {
      from: from,
      align: align
    }
  });
}

function powerOnMachine($systemId)
{
  showNotification('info', 'top', 'right', 'Maschine mit der ID ' + $systemId + ' wird gestartet');
  $.ajax({
    type: "POST",
    url: 'services/do_powerOnMachine.php',
    data: 'action=powerOn&sysId=' + $systemId,
    success: function(response) {
      let json = jQuery.parseJSON(response);
      return json.message;
    },
    error: function(error) {
      return error;
    }
  });
}

function deleteMachine($systemId)
{
  var ask = window.confirm('Bist du dir sicher, dass du die Machine mit der ID '+ $systemId +' löschen möchtest?');
  if (ask) {
    showNotification('danger', 'top', 'right', 'Maschine mit der ID ' + $systemId + ' wird gelöscht');
    $.ajax({
      type: "POST",
      url: 'services/do_deleteMachine.php',
      data: 'action=delete&sysId=' + $systemId,
      success: function(response) {
        let json = jQuery.parseJSON(response);
        return json.message;
      },
      error: function(error) {
        return error;
      }
    });
  }
}

function sendMail($data)
{
  showNotification('info', 'top', 'right', 'E-Mail wird versendet');
  $.ajax({
    type: "POST",
    url: 'services/do_sendMail.php',
    data: 'action=sendMail&' + $data,
    success: function(response) {
      return response;
    },
    error: function(error) {
      return error;
    }
  });
}