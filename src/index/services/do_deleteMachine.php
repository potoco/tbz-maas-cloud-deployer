<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');

if (isset($_POST) && $_POST['action'] == 'delete')
{
  $delete = deleteMachine(UBUNTUUSER, MAASADMIN, $_POST['sysId']);
  if ($delete)
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler beim Löschen der Maschine '. $_POST['sysid'] .' ist aufgetreten: '.$delete;
  }
}

print json_encode($return);