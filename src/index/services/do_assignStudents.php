<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');


if (isset($_POST) && $_POST['action'] == 'assignStudents')
{
  $studentsArr = unserialize($_POST['studentsArr']);

  $poolName = getRessourcePoolByZoneName(UBUNTUUSER, MAASADMIN, VPNZONENAME);
  $machines = getMachinesByPoolName(UBUNTUUSER, MAASADMIN, $poolName);
  foreach ($machines as $machineIndex => $machine)
  {
    foreach ($studentsArr as $studentIndex => $student)
    {
      // increment by 1 to format ip address later on
      $studentCount = ($studentIndex + 1);
      // assign system id to student
      if ($machineIndex == $studentIndex)
      {
        $student['system_id'] = $machine->system_id;
        $student['ipaddr']    = preg_replace('/-/', '.', preg_replace('/-0$/', '-'.($studentCount+DEFAULTOFFSET), $machine->zone->name));
      }
      
      array_shift($studentsArr);
      $studentsArr[] = $student;
    }
  }

  foreach ($studentsArr as $student)
    $addDesc[] = addMachineDescription(UBUNTUUSER, MAASADMIN, $student['system_id'], base64_encode(serialize($student)));

  $addDesc = array_unique($addDesc);

  if (sizeof($addDesc) === 1 && $addDesc[0] === 0)
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler bei der Studentenzuweisung der Maschinen ist aufgetreten: '.$addDesc;
  }
}

print json_encode($return);

