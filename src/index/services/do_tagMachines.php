<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');


if (isset($_POST) && $_POST['action'] == 'tagMachines')
{
  $tagMachines = tagMachines(UBUNTUUSER, MAASADMIN, VPNTAGNAME);
  if ($tagMachines === 0)
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler beim taggen der Maschinen ist aufgetreten: '.$tagMachines;
  }
}

print json_encode($return);