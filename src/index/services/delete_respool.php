<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');

if (isset($_POST) && $_POST['action'] == 'delete')
{
  $resourcePool = getRessourcePoolByZoneName(UBUNTUUSER, MAASADMIN, VPNZONENAME);
  if (!empty($resourcePool))
  {
    $deleted = deleteResourcePool(UBUNTUUSER, MAASADMIN, $resourcePool);
    if ($deleted === true)
    {
      $return['message'] = 'success';
    }
    else
    {
      $return['message'] = 'Ein Fehler beim löschen des Ressourcenpools ist aufgetreten: '.$deleted;
    }
  }
}

print json_encode($return);