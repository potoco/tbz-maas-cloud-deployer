<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');

if (isset($_POST) && $_POST['action'] == 'sendMail')
{
  $module = $_POST['module'];
  $mailTo = [
              $_POST['email']
            ];
  $mailSubject = 'Wireguard Config für das Modul '.$module;
  $mailBody = '
      <h3>'.trim(MAILTITLETXT).' '.$module.'</h3>
      <p>'.trim(MAILBODYTXT).' '.$_POST['ipAddr'].'</p>
  ';
  $fileName = $module.'-'.$_POST['vpnId'].'.conf';
  $filePath = $_SERVER['DOCUMENT_ROOT'].'/downloads/'.$_POST['vpnId'].'.conf';
  $fileAttachment = [
                      $fileName => $filePath
                    ];

  $sendMail = sendMail($mailTo, $mailSubject, $mailBody, $fileAttachment);

  if (preg_match('/250 OK queued/', print_r($sendMail)))
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler beim sender einer E-Mail ist aufgetreten: '.print_r($sendMail);
  }
}

print json_encode($return);