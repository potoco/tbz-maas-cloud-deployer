<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/inc/functions.php');

if (isset($_POST) && $_POST['action'] == 'powerOn')
{
  $powerOn = powerOnMachine(UBUNTUUSER, MAASADMIN, $_POST['sysId']);
  if ($powerOn === 0)
  {
    $return['message'] = 'success';
  }
  else
  {
    $return['message'] = 'Ein Fehler beim Starten der Maschine '. $_POST['sysid'] .' ist aufgetreten: '.$powerOn;
  }
}

print json_encode($return);