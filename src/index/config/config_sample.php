<?php

// Session Config
define('SESSIONEXPIRE',  3600);

// MAAS + Lernmaas Config
define('UBUNTUUSER',     'ubuntu');
define('MAASADMIN',      'ubuntu');
define('MAASURL',        'http://localhost:5240/MAAS/api/2.0');
define('MAASAPIKEY',     'yQcTSyDxUPJ...');

// vhosts Config
define('VHOSTS',         [
                          '8081' => '10-1-38-0',
                          '8082' => '10-2-38-0',
                          '8083' => '10-3-38-0',
                          '8084' => '10-4-38-0'
                         ]);