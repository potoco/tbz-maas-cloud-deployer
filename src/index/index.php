<?php
  // Dashboard - index.php
?>
<?php

include('template-parts/header.php');

$arr = [];
foreach (VHOSTS as $port => $vpnZoneName)
{
  $arrPool = [];
  $resourcePool      = getRessourcePoolByZoneName(UBUNTUUSER, MAASADMIN, $vpnZoneName);
  $arrPool['module'] = ($resourcePool) ? strtoupper(explode('-', $resourcePool)[0]) : null;
  $arrPool['class']  = ($resourcePool) ? strtoupper(explode('-', $resourcePool)[1]) : null;
  $arrPool['port']   = $port;
  $arrPool['net']    = preg_replace('/-/', '.', $vpnZoneName);
  $arrPool['link']   = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].':'.$port.'/reload.php';
  $arr[] = $arrPool;
}

?>

<div class="content" style="background-color: #f4f3ef;">

  <div class="row">
    <?php
    foreach ($arr as $pool)
    {
?>
    <div class="col-lg-3 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-body ">
          <div class="row">
            <div class="col-5 col-md-4">
              <div class="icon-big text-center icon-warning">
                <i class="nc-icon nc-hat-3 text-warning"></i>
              </div>
            </div>
            <div class="col-7 col-md-8">
              <div class="numbers">
                <p class="card-category">Port <?php echo (isset($pool['port'])) ? $pool['port'] : ''; ?></p>
                <p class="card-title"><?php echo (isset($pool['class'])) ? $pool['class'] : 'Frei'; ?></p>
                <p class="card-category" style="margin-bottom: 5px"><?php echo (isset($pool['module'])) ? $pool['module'] : '-'; ?></p>
                <p class="card-category"><?php echo (isset($pool['net'])) ? $pool['net'] : ''; ?>/24</p>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer" style="text-align: center;">
          <p class="card-category"></p>
          <p class="card-category"></p>
          <button id="go-to-vhost" type="button" class="btn btn-secondary " onclick="window.location.href='<?php echo $pool['link']; ?>';">Zum Dashboard</button>
        </div>
      </div>
    </div>
<?php
    }
    ?>
  </div>
</div> <!--content-->

<?php include('template-parts/footer.php'); ?>
