#!/bin/bash

cd ~/tbz-maas-cloud-deployer
git pull
cd ~

sudo cp -R ~/tbz-maas-cloud-deployer/src/index/* /var/www/index/
sudo cp -R ~/tbz-maas-cloud-deployer/src/vhost/* /var/www/8081/
sudo cp -R ~/tbz-maas-cloud-deployer/src/vhost/* /var/www/8082/
sudo cp -R ~/tbz-maas-cloud-deployer/src/vhost/* /var/www/8083/
sudo cp -R ~/tbz-maas-cloud-deployer/src/vhost/* /var/www/8084/

sudo chown www-data:www-data /var/www/8081/downloads/
sudo chown www-data:www-data /var/www/8082/downloads/
sudo chown www-data:www-data /var/www/8083/downloads/
sudo chown www-data:www-data /var/www/8084/downloads/