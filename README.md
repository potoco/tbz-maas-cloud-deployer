# TBZ MAAS Cloud Deployer

## Beschreibung
Der [TBZ MAAS Cloud Deployer](https://gitlab.com/potoco/tbz-maas-cloud-deployer) ist eine Abstrahierung der Bedienungsoberfläche von [MAAS](https://maas.io/), welches durch [Canonical](https://canonical.com/) entwickelt wird.
Mit Hilfe des Projekts [mc-b](https://github.com/mc-b)/[lernmaas](https://github.com/mc-b/lernmaas), welches als Schnittstelle zwischen [TBZ MAAS Cloud Deployer](https://gitlab.com/potoco/tbz-maas-cloud-deployer) und [MAAS](https://maas.io/) dient, werden mit einfachsten Mitteln Lernumgebungen für Studenten bereitgestellt. 

## Installation
Eine detaillierte [Installationsanleitung](doc/install.md) kann in den [Docs](doc) gefunden werden.

## Dokumentation
Weitere Informationen können aus den hier gelisteten Dokumenten entnommen werden.
* [Betriebshandbuch](doc/user-manual.md)

## Roadmap
Bis jetzt sind keine weiteren Funktionen geplant. Gerne können Ideen via `Issue Tracker` mitgeteilt werden.

## Contribution
Pull-Anfragen sind willkommen. Bei größeren Änderungen öffne bitte zuerst ein Thema, um zu besprechen, was geändert werden soll.

## License
[MIT](https://choosealicense.com/licenses/mit/)