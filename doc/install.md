# Installation

## Inhaltsverzeichnis
1. [Voraussetzungen](#voraussetzungen)
1. [Installation der Abhängigkeiten](#installation-der-abhängigkeiten)
1. [Installation TBZ MAAS Cloud Deployer](#installation-tbz-maas-cloud-deployer)
1. [Konfiguration des Systems](#konfiguration-des-systems)
1. [Konfiguration TBZ MAAS Cloud Deployer](#konfiguration-tbz-maas-cloud-deployer)
    1. [index](#index)
    1. [vhosts](#vhosts)


## Voraussetzungen
Folgende Voraussetzung müssen für die Installation von TBZ MAAS Cloud Deployer erfüllt sein:
* MAAS Region+Rack Controller
* Mind. 1 KVM Maschine - gelockt
* [MAAS](https://maas.io/)-Installation gemäss [Anleitung](https://github.com/mc-b/lernmaas/blob/master/doc/MAAS/Install.md) von [mc-b](https://github.com/mc-b)/[lernmaas](https://github.com/mc-b/lernmaas)
* Mind. 1 [Availability Zone](https://maas.io/docs/availability-zones) - Konfiguriert gemäss [Anleitung](https://github.com/mc-b/lernmaas/blob/master/doc/MAAS/Gateway.md#vpn) von [mc-b](https://github.com/mc-b)/[lernmaas](https://github.com/mc-b/lernmaas)
  * siehe auch Hilfsscripts [updateaz](https://github.com/mc-b/lernmaas/blob/fb86c7e60006829a2c84ab262b0deb5134751218/helper/README.md#updateaz) 


## Installation der Abhängigkeiten
1. Packages installieren
    ~~~
    sudo apt install -y apache2 php7.4 php7.4-curl php-xml
    ~~~


## Installation TBZ MAAS Cloud Deployer
1. Repository klonen
    ~~~
    git clone https://gitlab.com/potoco/tbz-maas-cloud-deployer.git
    ~~~
1. Virtueller Host `index` bereitstellen
    ~~~
    sudo mkdir /var/www/index
    sudo cp -R ~/tbz-maas-cloud-deployer/src/index/* /var/www/index/
    sudo cp ~/tbz-maas-cloud-deployer/helper/index.conf /etc/apache2/sites-available/
    sudo a2dissite 000-default.conf
    sudo a2ensite index.conf
    ~~~
1. Virtuelle Hosts `vhosts` für die verschiedenen Instanzen bereitstellen
    > Ersetze `<PORT>` durch den von dir gewünschten Port (Standard-Ports `8081`, `8082`, `8083`...). 
    > Wiederhole diese Schritte für jede verfügbare [AZ](https://maas.io/docs/availability-zones) - Jeder `vhost` mit einem unterschiedlichen `<PORT>`
    ~~~
    sudo mkdir /var/www/<PORT>
    sudo cp -R ~/tbz-maas-cloud-deployer/src/vhost/* /var/www/<PORT>
    sudo chown www-data:www-data /var/www/<PORT>/downloads/
    sudo cp ~/tbz-maas-cloud-deployer/helper/vhost.conf /etc/apache2/sites-available/<PORT>.conf
    sudo sed -i ‘s/8081/<PORT>/g’ /etc/apache2/sites-available/<PORT>.conf
    sudo a2ensite <PORT>.conf
    ~~~
1. Service `apache2` neustarten
    ~~~
    sudo systemctl restart apache2
    ~~~


## Konfiguration des Systems
> Diese Einstellungen können ein Sicherheitsrisiko darstellen und sollten nur in lokal bereitgestellten und vertrauenswürdigen Umgebungen angewendet werden.
1. Benutzer `www-data` der Gruppe `sudo` hinzufügen
    ~~~
    sudo usermod -aG sudo www-data
    ~~~
1. Benutzer `www-data` das Ausführen von Scripts ohne Passwortabfrage erlauben
    > **ACHTUNG:** Diese Einstellung sollte nur zu Testzwecken angewendet werden
    > Bei produktiver Inbetriebnahme wird empfohlen die Ausführung von Scripts auf das nötigste (`lernmaas - /usr/local/bin/`) einzuschränken
    ~~~
    sudo visudo

    > # Allow members of group sudo to execute any command
    > ...
    > %www-data ALL=(ALL:ALL) NOPASSWD: ALL
    ~~~


## Konfiguration TBZ MAAS Cloud Deployer
> Ersetzte `vi` durch den Editor deiner wahl.

### index
1. `index` konfigurieren
    ~~~
    sudo cp /var/www/index/config/config_sample.php /var/www/index/config/config.php 
    sudo vi /var/www/index/config/config.php
    ~~~
1. Gebe unter `UBUNTUUSER` den Benutzer des Systems an
    ~~~
    define('UBUNTUUSER',     'ubuntu');
    ~~~
1. Gebe unter `MAASADMIN` den MAAS Admin-Benutzer an
    ~~~
    define('MAASADMIN',      'ubuntu');
    ~~~
1. (optional) Gebe eine alternative `MAASURL` an
    ~~~
    define('MAASURL',        'http://<SERVER>:5240/MAAS/api/2.0');
    ~~~
1. Gebe den `apikey` mit Hilfe der [MAAS CLI](https://maas.io/docs/maas-cli#heading--log-in-required) aus
    ~~~
    sudo maas apikey --username=$PROFILE
    ~~~
1. Gebe unter `MAASAPIKEY` den `apikey` an
    ~~~
    define('MAASAPIKEY',     'yQcTSyDxUPJ...');
    ~~~
1. Gebe unter `VHOSTS` das Mapping der `PORTS` auf die gewünschten Namen der [AZ](https://maas.io/docs/availability-zones)s
    ~~~
    define('VHOSTS',         [
                              '8081' => '10-1-32-0',
                              '8082' => '10-2-32-0',
                              '8082' => '10-3-32-0',
                              '8082' => '10-4-32-0'
                             ]);
    ~~~

### vhosts
> Ersetze `<PORT>` durch den von dir gewünschten Port (Standard-Ports `8081`, `8082`, `8083`...). 
> Wiederhole diese Schritte für jede verfügbare [AZ](https://maas.io/docs/availability-zones) - Jeder `vhost` mit einem unterschiedlichen `<PORT>`

1. `vhost` konfigurieren
    ~~~
    sudo cp /var/www/<PORT>/config/config_sample.php /var/www/<PORT>/config/config.php 
    sudo vi /var/www/<PORT>/config/config.php
    ~~~
1. Gebe unter `UBUNTUUSER` den Benutzer des Systems an
    ~~~
    define('UBUNTUUSER',     'ubuntu');
    ~~~
1. Gebe unter `MAASADMIN` den MAAS Admin-Benutzer an
    ~~~
    define('MAASADMIN',      'ubuntu');
    ~~~
1. (optional) Gebe eine alternative `MAASURL` an
    ~~~
    define('MAASURL',        'http://<SERVER>:5240/MAAS/api/2.0');
    ~~~
1. Gebe den `apikey` mit Hilfe der [MAAS CLI](https://maas.io/docs/maas-cli#heading--log-in-required) aus
    ~~~
    sudo maas apikey --username=$PROFILE
    ~~~
1. Gebe unter `MAASAPIKEY` den `apikey` an
    ~~~
    define('MAASAPIKEY',     'yQcTSyDxUPJ...');
    ~~~
1. Gebe unter `VPNZONENAME` den Namen der zugewiesenen [AZ](https://maas.io/docs/availability-zones) an
    ~~~
    define('VPNZONENAME',     '10-1-32-0');
    ~~~
1. Konfiguriere alle nötigen E-Mail Einstellungen für den Mailer Dienst
    ~~~
    define('SMTPSERVER',   'smtp.domain.tld');
    define('SMTPPORT',     '587');
    define('SMTPAUTH',      true);
    define('SMTPAUTHUSER', 'user');
    define('SMTPAUTHPASS', 'pass');
    define('MAILFROM',     'mail@domain.tld');
    ~~~
1. Für weitere Konfigurationen kann das [Betriebshandbuch](user-manual.md) eingesehen werden.
