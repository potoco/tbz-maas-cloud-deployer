# Betriebshandbuch

## Inhaltsverzeichnis
1. [Allgemeine Hinweise](#allgemeine-hinweise)
1. [Limitierungen](#limitierungen)
1. [Zu erwartende Wartezeiten](#zu-erwartende-wartezeiten)
1. [Das UI](#das-ui)
    1. [Startseite](#startseite)
    1. [Dashboard](#dashboard)
1. [Bedienung des TBZ-MCD](#bedienung-des-tbz-mcd)
    1. [Erstellen einer Lernumgebung](#erstellen-einer-lernumgebung)
        1. [Klassenliste einlesen](#klassenliste-einlesen)
        1. [Manuell erstellen](#manuell-erstellen)
    1. [Löschen einer Lernumgebung](#löschen-einer-lernumgebung)
    1. [Aktualisierung der Daten](#aktualisierung-der-daten)
    1. [Studenten und VPN-Konfigurationen](#studenten-und-vpn-konfigurationen)
        1. [VPN-Konfiguration anwenden (Student)](#vpn-konfiguration-anwenden-student)
    1. [Weitere Aktionen](#weitere-aktionen)
        1. [Fall: Schüler fährt Maschine herunter](#fall-schüler-fährt-maschine-herunter)
        1. [Fall: Eine VM soll gelöscht werden](#fall-eine-vm-soll-gelöscht-werden)
    1. [Support](#support)
1. [Erweitere Konfiguration (für Administratoren)](#erweitere-konfiguration-für-administratoren)
    1. [E-Mail Template anpassen](#e-mail-template-anpassen)
    1. [VPN IP-Adressen anpassen](#vpn-ip-adressen-anpassen)
1. [Abkürzungsverzeichnis](#abkürzungsverzeichnis)

## Allgemeine Hinweise
* Es ist nicht empfohlen, mehrere Klassen im selben Browser zu erstellen oder zu administrieren. Die Daten der Klasse werden mittels Session-Cookie zwischengespeichert, dadurch kann es zu abweichenden oder nicht korrekten Angaben im UI kommen.
* Die [Wartezeiten](#zu-erwartende-wartezeiten) müssen bekannt sein und eingehalten werden.
* Klassenlisten werden ausschliesslich nur von [ecolm.com](https://ecolm.com/) unterstützt. Klassenlisten, welche im Aufbau des CSV abweichen, können ohne Gewähr ausprobiert werden.

## Limitierungen
Folgende limitierungen weisst der TBZ-MCD zum jetzigen Entwicklungsstand auf.
* Es werden so viele Lernumgebungen unterstützt wie [AZ](https://maas.io/docs/availability-zones)s auf dem MAAS Controller konfiguriert sind.
* Es werden bis zu 30 virtuelle Maschinen pro Lernumgebung unterstützt.
    * Die Anzahl VMs pro Lernumgebungen müssen durch die Anzahl KVM Maschinen bzw. deren bereitgestellten Ressourcen unterstützt sein. (Formel: Total VMs (alle Umgebungen) x Ø2GB RAM = Total RAM aller KVM Maschinen zusammen)
    * Bsp. 4 Lernumgebungen à 30 VMs * Ø2GB RAM / 5 [Anzahl KVM Maschinen] = 48GB --> Jeder KVM Host sollte mind. 48GB RAM aufweisen
* Spezifische MAAS Funktionen, welche nicht im TBZ-MCD abgebildet wurden, sind nicht unterstützt
* Die angezeigten Daten im UI werden zwischengespeichert und müssen [manuell aktualisiert](#aktualisierung-der-daten) werden.

## Zu erwartende Wartezeiten
Nachfolgend sind die zu erwartenden Wartezeiten pro Aufgabe aufgelistet. Es ist wichtig diese zu kennen, da es im UI den Anschein haben kann, dass etwas hängen geblieben ist.
Solange diese Zeiten nicht übermässig überschritten wurden, arbeitet der TBZ-MCD daran die gewünschte Lernumgebung bereitzustellen.

> **WICHTIG:** Während der Bereitstellung darf der Browser bzw. die Seite weder aktualisiert noch neu geladen oder verlassen werden.
> Diese Aktionen können zu einer fehlerhaften Bereitstellung führen.

Aktion | Anzahl Maschinen | Dauer
------ | ---------------- | -----
Reload | 1-30 | ~15 Sekunden
Erstellen | ~5 | ~7 Minuten
Erstellen | ~15 | ~10 Minuten
Erstellen | 30 | ~15 Minuten
Löschen | ~5 | ~30 Sekunden
Löschen | ~15 | ~1 Minuten
Löschen | 30 | ~2 Minuten

## Das UI
### Startseite
Die Startseite von TBZ-MCD kann mittels `IP-Adresse` oder `Hostnamen` (je nach Konfiguration des Administrators) über einen beliebigen Webbrowser erreicht werden.
Auf der Startseite sind die verfügbaren Lernumgebungen zu sehen, welche Klasse mit welchem Modul auf der Lernumgebungen bereitgestellt ist, welches VPN diese Lernumgebungen nutzt und auf welchem Port diese zur Verfügung stehen.
Freie Lernumgebungen werden mit `Frei` gekennzeichnet. Durch einen Klick auf `ZUM DASHBOARD` gelangt man auf die entsprechende Lernumgebung.

![Startseite UI](img/startseite_ui.png)

> **Legende**
> 
> 1. Übersicht aller zur Verfügung stehenden Lernumgebungen
> 2. Klasse und Module
> 3. Freie Umgebung
> 4. Verwendetes VPN / Netzwerk
> 5. Port auf dem das Dashboard der Umgebung erreicht werden kann
> 6. Link zum Navigieren auf das gewünschte Dashboard


### Dashboard
Auf dem Dashboard der Lernumgebung sind die wichtigsten Informationen zu sehen.
Links befindet sich die Navigationsleiste, diese erlaubt das Navigieren zur Übersicht aller Studenten mit den jeweiligen VPN-Konfigurationen, sowie eine detaillierte Einsicht in die darunterliegende MAAS Infrastruktur.
Auf der rechten Seite sind Informationen zur Klasse und Modul zu sehen, sowie der verwendete MAAS Rack Controller, welcher zu Supportzwecken angegeben werden kann. Über den Aktionsbutton `ERSTELLEN` lässt sich eine Lernumgebung erstellen.
Zuoberst rechts befindet sich noch der `RELOAD`-Button, dieser wird verwendet um die zwischengespeicherten Daten neu von der MAAS-Infrastrutkur abzuholen und im UI zu aktualisieren.
Weitere Informationen sind in den nachfolgenden Kapiteln unter [Bedienung des TBZ-MCD](#bedienung-des-tbz-mcd) zu entnehmen.

![Dashboard](img/dashboard.png)

> **Legende**
> 
> 1. Navigationsmenu
> 2. Klasse
> 3. Modul
> 4. MAAS Controller
> 5. Aktionsbutton um Lernumgebung zu Erstellen oder zu Löschen
> 6. Reload-Button zum Aktualisieren der angezeigten Daten
> 7. Auflistung aller Maschinen


## Bedienung des TBZ-MCD
### Erstellen einer Lernumgebung
1. Über den Button oben rechts `Erstellen` gelangt man zum Formular zur Erstellung einer neuen Lernumgebungen.
    ![Erstellen](img/erstellen.png)
1. Es stehen zwei verschiedene Varianten zur Erstellung einer Lernumgebungen zur Verfügung:
    * `(1)` Klassenliste - wird anhand einer Klassenliste von [ecolm.com](https://ecolm.com) erstellt 
    * `(2)` Manuell - wird anhand manuellen Angaben erstellt

    ![Varianten zum Erstellen](img/varianten-erstellen.png)

    #### Klassenliste einlesen
    1. Wähle die Klassenliste `(1)` von [ecolm.com](https://ecolm.com) und lade diese mittels Upload-Button `(2)` hoch                       
        ![Upload Klassenlise](img/upload-klassenliste.png)
    1. Validiere die entnommenen Informationen aus der Klassenliste
        ![Eval Students](img/val-students.png)
    1. Fülle die Angaben zur Klasse und Modul aus und bestätige mit `LERNUMGEBUNG ERSTELLEN` 
        ![Submit Klassenlise](img/submit-klassenliste.png)

    #### Manuell erstellen
    > **Hinweis:** Bei manueller Erstellung werden die Maschinen nicht den Studenten zugewiesen.
    > VPN-Konfiguration müssen manuell den Stunden ausgeteilt werden.
    1. Wähle die Option `Manuell erstellen`
    1. Fülle die Angaben zur Klasse, Modul und Anzahl Studenten aus und bestätige mit `LERNUMGEBUNG ERSTELLEN`
        ![Manuell erstellen](img/create-manual.png)

&nbsp; 


3. Während der Erstellung der Lernumgebung gelten die bereits erwähnten [Wartezeiten](#zu-erwartende-wartezeiten).
    Der Fortschritt der Erstellung der Lernumgebung kann anhand der Progress-Bar `(1)` und dem Text-Feedback `(2)` entnommen werden.
    ![Create progress](img/create-progress.png)

1. Sobald die Lernumgebung fertiggestellt ist, kann man zum Dashboard zurückkehren
    ![Create finished](img/create-finished.png)

### Löschen einer Lernumgebung
Lernumgebungen lassen sich noch einfacher wieder Löschen. Dazu reicht ein Klick auf den Aktionsbutton oben recht `LÖSCHEN` und die Bestätigung des Löschens.
Auch hier wird der Fortschritt des Löschens angezeigt. Hier gelten ebenfalls die bereits erwähnten [Wartezeiten](#zu-erwartende-wartezeiten).

![Create finished](img/delete-lernmaas.png)
![Create finished](img/delete-progress.png)


### Aktualisierung der Daten
Die Daten der Umgebung werden im Session-Cookie zwischengespeichert. Sollten die im UI angezeigten Daten nicht korrekt sein, können diese mittels
`RELOAD`-Button aktualisiert werden. Es wird empfohlen diesen Button regelmässig aufzusuchen, um sicherzustellen, dass alle Angaben aktuell sind.

![Reload Button](img/reload-button.png)

### Studenten und VPN-Konfigurationen
Über die Navigation auf der linken Seite gelangt man auf die Übersicht der Studenten.
Diese Seite zeigt auf den Studenten bezogene Informationen an. Zu sehen ist, welche Maschine dem Studenten zugewiesen ist,
die IP-Adresse der Maschine und bietet ausserdem die Möglichkeit die VPN-Konfiguration des Studenten (für [Wireguard](https://www.wireguard.com/)) einzusehen
und sogar dem Studenten via E-Mail zukommen zu lassen.

Über den Download-Button `(1)` kann die VPN-Konfiguration des Studenten eingesehen bzw. heruntergeladen werden.

Über den E-Mail-Button `(2)` kann die VPN-Konfiguration des Studenten an seine E-Mail Adresse gesendet werden.

![Studenten Buttons](img/student-buttons.png)

#### VPN-Konfiguration anwenden (Student)
1. Falls noch nicht installiert - Installiere [Wireguard](https://www.wireguard.com/install/)
1. Speichere die via E-Mail erhaltene VPN-Konfigurationsdatei auf deinem Gerät
1. Füge die VPN-Konfigurationsdatei im Wireguard-Client hinzu und aktiviere diese
    ![Wireguard Config](img/wireguard-config.png)
1. Nun kann auf die IP-Adresse der Maschine mittels SSH zugegriffen werden - Login-Informationen werden von der Lehrperson mitgeteilt. 

### Weitere Aktionen
In der Liste der Maschinen auf dem Dashboard stehen weitere Aktionen pro Maschine zur Verfügung. Je nach Fall, ob ein Student eine Maschine aus Versehen heruntergefahren hat oder eine Maschine nicht mehr benötigt wird, können diese sehr hilfreich sein.

#### Fall: Student fährt Maschine herunter
Als erstes sollte sichergestellt werden, dass die Daten aktualisiert sind. Klicke auf den `RELOAD`-Button um die heruntergefahrene Maschine zu identifizieren.
Danach kann der `PLAY`-Button der Maschine betätigt werden, dies löst eine Meldung oben rechts aus und die Maschine wird gestartet. Vergiss nicht, die Daten erneut mittels `RELOAD`-Button zu aktualisieren.

![Maschine starten](img/start-machine.png)

#### Fall: Eine VM soll gelöscht werden
Sollte eine VM zu viel erstellt worden sein, kann diese einfach über den `LÖSCHEN`-Button gelöscht werden. Auch hier müssen nach dem Löschen die aktuellen Daten neu mittels `RELOAD`-Button abgeholt werden.

![Maschine loeschen](img/delete-machine.png)

### Support
Sollte der Fall eintreffen, dass Support benötigt wird, helfen die Angaben zur MAAS-Infrastruktur. Diese sind in der Navigation auf der linken Seite unter MAAS Rack zu finden.

![MAAS Rack](img/maas-rack-info.png)


## Erweitere Konfiguration (für Administratoren)
### E-Mail Template anpassen
Das E-Mail Template, welches für den Versand von VPN-Konfigurationsdateien an Studenten verwendet wird sieht standardmässig folgendermassen aus:
![E-Mail Template](img/mail-template.png)

Alle Texte (`MAILTITLETXT` + `MAILBODYTXT`) und Anzeigenamen (`MAILFROMNAME`) lassen sich in der Konfiguration der jeweiligen `vhosts` anpassen (siehe [Installationsanleitung](install.md))
E-Mails werden im HTML-Format versendet, somit können diese entsprechend im HTML-Syntax gestaltet werden. 

Mit den Parametern `MAILCC` und `MAILBCC` können weitere E-Mail-Adressen eintragen werden und so den Versand bzw. den Erhalt der VPN-Konfiguration kontrolliert und nachverfolgt werden.

> Der Betreff der E-Mail kann nicht angepasst werden. Dieser wird mit dem von der Lehrperson angegebenen Modul versehen.

~~~
Datei: /var/www/<PORT>/config/config.php
~~~

~~~
define('MAILFROMNAME', 'TBZ MAAS Cloud Deployer');
define('MAILCC',       ''); // Lehrperson oder Mailservice zwecks Kontrolle
define('MAILBCC',      ''); // Lehrperson oder Mailservice  zwecks Kontrolle
define('MAILTITLETXT', 'Deine Wireguard Config für das Modul'); // Modulname wird dynamisch geladen
define('MAILBODYTXT',  'Füge die Wireguard-Konfigurationsdatei in deinem Wireguard-Client hinzu.<br>Die IP-Adresse deiner Maschine lautet:');
~~~

### VPN IP-Adressen anpassen
Die VPN Adressen werden standardmässig in zwei Bereiche aufgeteilt (nachfolgend der Hostanteil eines /24 Netzes): 
* Maschinen: 11-40 (max. 30 Maschinen pro Lernumgebung)
* Studenten: 51-80 (max. 30 Studenten pro Lernumgebung)

Diese IP-Bereiche lassen sich in der Konfiguration der jeweiligen `vhosts` anpassen (siehe [Installationsanleitung](install.md)).
~~~
Datei: /var/www/<PORT>/config/config.php
~~~

~~~
define('DEFAULTOFFSET',  '10'); // Maschinen - nächste IP wird bezogen (11)
define('STUDENTSOFFSET', '50'); // Studenten - nächste IP wird bezogen (51)
~~~

## Abkürzungsverzeichnis
Abkürzung | Bedeutung
--------- | ---------
UI | User Interface (Webbrowser Ansicht)
TBZ | Techn. Berufsschule Zürich
TBZ-MCD | TBZ MAAS Cloud Deployer
CSV | Comma-/Character-separated values
VM | Virtuelle Maschine
VPN | Virtual Private Network
MAAS | Metal as a Service
HTML | Hyper Text Markup Language